<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 01.04.2022
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Клиенты</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>

<body>
<c:if test="${err!=null}">
    <script>
        alert("${err}");
    </script>
</c:if>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/cars">Автомобили</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="/owners">Клиенты</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/contracts">Договоры</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/priceList">Прайс-лист</a>
        </li>
    </ul>
</nav>

<p></p>

<p>
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addOrEditOwner"
            style="margin-left: 10%;" onclick="addTitleInModalHeaderAndButton()">Добавить
    </button>
</p>

<div class="modal" id="addOrEditOwner">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="owners" enctype="text/html;charset=UTF-8">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="modalTitle"></h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="clearFillInputsOwnerModal()">×
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="fio">ФИО:</label>
                        <input required type="text" class="form-control" id="fio" name="fio" maxlength="50"
                               pattern="[A-Za-zА-Яа-яЁё\s]+$" title="ФИО должно содержать только буквы"
                               placeholder="Иванов Иван Иванович">
                    </div>
                    <div class="form-group">
                        <label for="phone">Телефон:</label>
                        <input required type="text" class="form-control" id="phone" name="phone" maxlength="12"
                               pattern="\+7[0-9]{10}" title="Введите номер телефона, начиная с +7"
                               placeholder="Введите номер в формате +79171112201">
                    </div>
                    <div class="form-group">
                        <label for="passportNumber">Номер паспорта:</label>
                        <input required type="text" class="form-control" id="passportNumber" name="passportNumber"
                               maxlength="10"
                               pattern="[0-9]{10}" title="Номер паспорта может содержать только цифры"
                               placeholder="Введите номер паспорта">
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="hidden" class="form-control" id="id" name="id">
                    <button type="submit" class="btn btn-success" id="addOrSaveButton">Сохранить</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"
                            onclick="clearFillInputsOwnerModal()">Отмена
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<table id="owners" class="table table-striped" style="width: 80%;" align="center">
    <tr>
        <th style="width: 43%;">Фамилия Имя Отчество</th>
        <th style="width: 20%;">Телефон</th>
        <th style="width: 20%;">Номер паспорта</th>
        <th style="width: 17%;">Действия</th>
    </tr>

    <c:forEach var="owner" items="${owners}">
        <tr>
            <td>${owner.getFio()}</td>
            <td>${owner.getPhone()}</td>
            <td>${owner.getPassportNumber()}</td>
            <td>
                <button type="button" class="btn btn-warning btn-sm"
                        data-id="${owner.getId()}"
                        data-fio="${owner.getFio()}"
                        data-phone="${owner.getPhone()}"
                        data-passport="${owner.getPassportNumber()}"
                        onclick="fillEditOwnerModal(this)"
                        data-toggle="modal"
                        data-target="#addOrEditOwner">Изменить
                </button>
                <button type="button" class="btn btn-danger btn-sm"
                        data-id="${owner.getId()}"
                        onclick="fillRemoveOwnerModal(this)"
                        data-toggle="modal"
                        data-target="#removeOwner">Удалить
                </button>
            </td>
        </tr>
    </c:forEach>
</table>

<div class="modal" id="removeOwner">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="POST" action="owners" enctype="text/html;charset=UTF-8">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">При удалении клиента будут удалены все связанные с ним договоры. Вы хотите
                        продолжить?</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <%--<div class="modal-body">

                </div>--%>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="hidden" class="form-control" id="removeId" name="removeId">
                    <button type="submit" class="btn btn-success">Да</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Нет</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function addTitleInModalHeaderAndButton() {
        document.getElementById("modalTitle").innerText = "Добавить владельца";
        document.getElementById("addOrSaveButton").innerText = "Добавить";
    }

    function fillEditOwnerModal(but) {
        document.getElementById("modalTitle").innerText = "Изменить владельца";
        document.getElementById("addOrSaveButton").innerText = "Сохранить";
        document.getElementById('id').value = but.dataset.id;
        document.getElementById('fio').value = but.dataset.fio;
        document.getElementById('phone').value = but.dataset.phone;
        document.getElementById('passportNumber').value = but.dataset.passport;
    }

    function fillRemoveOwnerModal(but) {
        document.getElementById('removeId').value = but.dataset.id;
    }

    function clearFillInputsOwnerModal() {
        document.getElementById('id').value = "";
        document.getElementById('fio').value = "";
        document.getElementById('phone').value = "";
        document.getElementById('passportNumber').value = "";
    }
</script>
</body>
</html>
