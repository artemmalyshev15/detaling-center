package dao;

import interfaces.DAO;
import model.PriceList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class PriceListDAOImpl implements DAO<PriceList> {

    private final SessionFactory sessionFactory;

    public PriceListDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PriceList findById(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(PriceList.class, id);
        }
    }

    @Override
    public void save(PriceList priceList) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(priceList);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void update(PriceList priceList) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(priceList);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void delete(PriceList priceList) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(priceList);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public List<PriceList> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return (List<PriceList>) session.createQuery("from PriceList order by id").list();
        }
    }

    public List<PriceList> findByWorkName(String workName) {
        try (Session session = sessionFactory.openSession()) {
            return (List<PriceList>) session.createQuery("from PriceList where workName = :workName")
                    .setParameter("workName", workName).list();
        }
    }
}
