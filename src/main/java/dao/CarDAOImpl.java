package dao;

import interfaces.DAO;
import model.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class CarDAOImpl implements DAO<Car> {

    private final SessionFactory sessionFactory;

    public CarDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Car findById(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Car.class, id);
        }
    }

    @Override
    public void save(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(car);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public void update(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.update(car);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public void delete(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.delete(car);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public List<Car> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return (List<Car>) session.createQuery("from Car order by id").list();
        }
    }
}
