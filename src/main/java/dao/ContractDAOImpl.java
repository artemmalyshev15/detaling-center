package dao;

import interfaces.DAO;
import model.Contract;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ContractDAOImpl implements DAO<Contract> {

    private final SessionFactory sessionFactory;

    public ContractDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Contract findById(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Contract.class, id);
        }
    }

    @Override
    public void save(Contract contract) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.save(contract);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public void update(Contract contract) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.update(contract);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public void delete(Contract contract) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx1 = session.beginTransaction();
            session.delete(contract);
            tx1.commit();
            session.close();
        }
    }

    @Override
    public List<Contract> getAll() {
        try (Session session = sessionFactory.openSession()) {
            List<Contract> contracts = session.createQuery("from Contract order by id").list();
            for (Contract contract : contracts) {
                Hibernate.initialize(contract.getCar());
                Hibernate.initialize(contract.getOwner());
                Hibernate.initialize(contract.getPriceLists());
            }
            return contracts;
        }
    }
}
