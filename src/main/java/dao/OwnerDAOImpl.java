package dao;

import interfaces.DAO;
import model.Owner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class OwnerDAOImpl implements DAO<Owner> {

    private final SessionFactory sessionFactory;

    public OwnerDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Owner findById(long id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Owner.class, id);
        }
    }

    @Override
    public void save(Owner owner) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(owner);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void update(Owner owner) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(owner);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public void delete(Owner owner) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(owner);
            transaction.commit();
            session.close();
        }
    }

    @Override
    public List<Owner> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return (List<Owner>) session.createQuery("from Owner order by id").list();
        }
    }

    public List<Owner> findOwnersByPassportNumber(String passportNumber) {
        try (Session session = sessionFactory.openSession()) {
            return (List<Owner>) session.createQuery("from Owner where passportNumber = :passportNumber")
                    .setParameter("passportNumber", passportNumber).list();
        }
    }
}
