package service;

import dao.PriceListDAOImpl;
import exceptions.DuplicateDataExceptions;
import model.PriceList;

import java.util.List;

public class PriceListService {

    private final PriceListDAOImpl priceListDAO;

    public PriceListService(PriceListDAOImpl priceListDAO) {
        this.priceListDAO = priceListDAO;
    }

    public PriceList findPriceListById(long id) {
        return priceListDAO.findById(id);
    }

    public void savePriceList(PriceList priceList) {
        if (priceListDAO.findByWorkName(priceList.getWorkName()).stream().count() != 0) {
            throw new DuplicateDataExceptions("Работа с таким названием уже существует. " +
                    "Название работы должно быть уникальным. Проверьте правильность ввода данных.");
        }
        priceListDAO.save(priceList);
    }

    public void updatePriceList(PriceList priceList) {
        if (priceListDAO.findByWorkName(priceList.getWorkName()).stream().count() != 0) {
            throw new DuplicateDataExceptions("Работа с таким названием уже существует. " +
                    "Название работы должно быть уникальным. Проверьте правильность ввода данных.");
        }
        priceListDAO.update(priceList);
    }

    public void deletePriceList(PriceList priceList) {
        priceListDAO.delete(priceList);
    }

    public List<PriceList> getAllPriceList() {
        return priceListDAO.getAll();
    }

}
