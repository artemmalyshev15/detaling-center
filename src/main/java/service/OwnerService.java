package service;

import dao.OwnerDAOImpl;
import exceptions.DuplicateDataExceptions;
import model.Owner;

import java.util.List;

public class OwnerService {

    private final OwnerDAOImpl ownerDAO;

    public OwnerService(OwnerDAOImpl ownerDAO) {
        this.ownerDAO = ownerDAO;
    }

    public Owner findOwnerById(long id) {
        return ownerDAO.findById(id);
    }

    public void saveOwner(Owner owner) {
        if (ownerDAO.findOwnersByPassportNumber(owner.getPassportNumber()).stream().count() != 0) {
            throw new DuplicateDataExceptions("Клиент с таким номер паспорта уже существует. " +
                    "Номер паспорта должен быть уникален. Проверьте данные еще раз.");
        }
        ownerDAO.save(owner);
    }

    public void updateOwner(Owner owner) {
        if (ownerDAO.findOwnersByPassportNumber(owner.getPassportNumber()).stream().count() != 0) {
            throw new DuplicateDataExceptions("Клиент с таким номер паспорта уже существует. " +
                    "Номер паспорта должен быть уникален. Проверьте данные еще раз.");
        }
        ownerDAO.update(owner);
    }

    public void deleteOwner(Owner owner) {
        ownerDAO.delete(owner);
    }

    public List<Owner> getAllOwners() {
        return ownerDAO.getAll();
    }

}
