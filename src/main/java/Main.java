import dao.CarDAOImpl;
import dao.ContractDAOImpl;
import dao.OwnerDAOImpl;
import dao.PriceListDAOImpl;
import model.Car;
import model.Contract;
import model.Owner;
import model.PriceList;
import org.hibernate.SessionFactory;
import service.CarService;
import service.ContractService;
import service.OwnerService;
import service.PriceListService;
import sessionUtil.HibernateSessionFactoryUtil;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        try {
            SessionFactory sessionFactory = HibernateSessionFactoryUtil.createSessionFactory();

            CarService carService = new CarService(new CarDAOImpl(sessionFactory));

            Car car1 = new Car("м707мм763", "bmw m2", "Угольно черный", LocalDate.of(2011, Month.NOVEMBER, 2));
            Car car2 = new Car("у283мм163", "bmw x5m", "Сан-Марино Блю", LocalDate.of(2011, Month.NOVEMBER, 2));
            Car car3 = new Car("у249рм163", "hyundai ix35", "Black Onyx Pearl", LocalDate.of(2014, Month.DECEMBER, 14));
            Car car4 = new Car("р826дт163", "toyota land cruiser 200", "темно-серый", LocalDate.of(2007, Month.MAY, 5));

            carService.saveCar(car1);
            carService.saveCar(car2);
            carService.saveCar(car3);
            carService.saveCar(car4);

            OwnerService ownerService = new OwnerService(new OwnerDAOImpl(sessionFactory));

            Owner owner1 = new Owner("Иванов Денис Андреевич", "+79171169785", "3614207658");
            Owner owner2 = new Owner("Петров Иван Сергеевич", "+79871132516", "3614207950");
            Owner owner3 = new Owner("Васильев Артем Сергеевич", "+79173066168", "3614205781");
            Owner owner4 = new Owner("Сидоров Виктор Евгеньевич", "+79175042104", "3614200999");

            ownerService.saveOwner(owner1);
            ownerService.saveOwner(owner2);
            ownerService.saveOwner(owner3);
            ownerService.saveOwner(owner4);

            PriceListService priceListService = new PriceListService(new PriceListDAOImpl(sessionFactory));

            PriceList priceList1 = new PriceList("Оклейка защитной пленкой", "от 49999,99 руб.");
            PriceList priceList2 = new PriceList("Детейлинг мойка", "от 1999,99 руб.");
            PriceList priceList3 = new PriceList("Химчистка салона", "от 4999,99 руб.");
            PriceList priceList4 = new PriceList("Окраска", "от 20000 руб.");
            PriceList priceList5 = new PriceList("Мойка подвески", "от 15000 руб.");

            priceListService.savePriceList(priceList1);
            priceListService.savePriceList(priceList2);
            priceListService.savePriceList(priceList3);
            priceListService.savePriceList(priceList4);
            priceListService.savePriceList(priceList5);

            List<PriceList> priceLists1 = new ArrayList<>();
            priceLists1.add(priceList1);
            priceLists1.add(priceList3);
            priceLists1.add(priceList4);

            List<PriceList> priceLists2 = new ArrayList<>();
            priceLists2.add(priceList3);
            priceLists2.add(priceList5);

            List<PriceList> priceLists3 = new ArrayList<>();
            priceLists3.add(priceList1);
            priceLists3.add(priceList3);

            List<PriceList> priceLists4 = new ArrayList<>();
            priceLists4.add(priceList2);
            priceLists4.add(priceList5);

            List<PriceList> priceLists5 = new ArrayList<>();
            priceLists5.add(priceList2);
            priceLists5.add(priceList4);

            ContractService contractService = new ContractService(new ContractDAOImpl(sessionFactory));

            Contract contract1 = new Contract(LocalDate.of(2022, 3, 15), LocalDate.of(2022, 3, 15), car1, owner1, priceLists1);
            Contract contract2 = new Contract(LocalDate.of(2022, 3, 15), LocalDate.of(2022, 3, 19), car1, owner1, priceLists2);
            Contract contract3 = new Contract(LocalDate.of(2022, 3, 15), LocalDate.of(2022, 4, 2), car2, owner2, priceLists3);
            Contract contract4 = new Contract(LocalDate.of(2022, 3, 15), LocalDate.of(2022, 3, 27), car3, owner3, priceLists4);
            Contract contract5 = new Contract(LocalDate.of(2022, 3, 15), LocalDate.of(2022, 4, 25), car4, owner4, priceLists5);

            contractService.saveContract(contract1);
            /*contractService.saveContract(contract2);*/
            contractService.saveContract(contract3);
            contractService.saveContract(contract4);
            contractService.saveContract(contract5);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
