package sessionUtil;

import model.Car;
import model.Contract;
import model.Owner;
import model.PriceList;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {

    public static SessionFactory createSessionFactory() {
        try {
            Configuration configuration = new Configuration().configure();
            configuration.addAnnotatedClass(Car.class);
            configuration.addAnnotatedClass(Owner.class);
            configuration.addAnnotatedClass(Contract.class);
            configuration.addAnnotatedClass(PriceList.class);
            StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties());
            return configuration.buildSessionFactory(builder.build());
        } catch (Exception e) {
            System.out.println("Ошибка: " + e.getMessage());
            throw e;
        }
    }
}
