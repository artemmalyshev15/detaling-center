package model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@ToString(exclude = "priceLists")
@Entity
@Table(name = "Contract")
@SequenceGenerator(name = "SEQ_CONTRACT_ID", sequenceName = "SEQ_CONTRACT_ID", allocationSize = 1)
public class Contract {

    @Id
    @GeneratedValue(generator = "SEQ_CONTRACT_ID")
    private long id;

    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "car_id", nullable = false)
    private Car car;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private Owner owner;

    @ManyToMany
    @JoinTable(name = "List_works",
            joinColumns = @JoinColumn(name = "contract_id"),
            inverseJoinColumns = @JoinColumn(name = "price_list_id")
    )
    private List<PriceList> priceLists;

    public Contract(LocalDate startDate, LocalDate endDate, Car car, Owner owner) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.car = car;
        this.owner = owner;
    }

    public Contract(LocalDate startDate, LocalDate endDate, Car car, Owner owner, List<PriceList> priceLists) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.car = car;
        this.owner = owner;
        this.priceLists = priceLists;
    }

    public Contract(long id, LocalDate startDate, LocalDate endDate, String regNumber, String brand, String fio) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.car = new Car(regNumber, brand);
        this.owner = new Owner(fio);
    }

    public String printPriceLists() {
        return getPriceLists().stream().map(p -> p.getWorkName()).collect(Collectors.joining("; "));
    }
}
