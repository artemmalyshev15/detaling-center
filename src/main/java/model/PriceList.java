package model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(exclude = "listWorks")
@Entity
@Table(name = "Price_list")
@SequenceGenerator(name = "SEQ_PRICE_ID", sequenceName = "SEQ_PRICE_ID", allocationSize = 1)
public class PriceList {

    @Id
    @GeneratedValue(generator = "SEQ_PRICE_ID")
    private long id;

    @Column(name = "work_name", length = 50, unique = true, nullable = false)
    private String workName;

    @Column(name = "price", nullable = false, length = 20)
    private String price;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "priceLists", cascade = CascadeType.ALL)
    private List<Contract> listWorks;

    public PriceList(String workName, String price) {
        this.workName = workName;
        this.price = price;
    }

    public PriceList(String workName, String price, List<Contract> listWorks) {
        this.workName = workName;
        this.price = price;
        this.listWorks = listWorks;
    }
}
