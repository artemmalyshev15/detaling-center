package model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(exclude = "contracts")
@Entity
@Table(name = "Owner")
@SequenceGenerator(name = "SEQ_OWNER_ID", sequenceName = "SEQ_OWNER_ID", allocationSize = 1)
public class Owner {

    @Id
    @GeneratedValue(generator = "SEQ_OWNER_ID")
    private long id;

    @Column(name = "fio", length = 50, nullable = false)
    private String fio;

    @Column(name = "phone", length = 12, nullable = false)
    private String phone;

    @Column(name = "passport_number", length = 10, unique = true, nullable = false)
    private String passportNumber;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner", cascade = CascadeType.ALL)
    private List<Contract> contracts;

    public Owner(String fio, String phone, String passportNumber) {
        this.fio = fio;
        this.phone = phone;
        this.passportNumber = passportNumber;
    }

    public Owner(String fio) {
        this.fio = fio;
    }
}
