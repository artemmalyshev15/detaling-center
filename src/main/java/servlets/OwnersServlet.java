package servlets;

import dao.OwnerDAOImpl;
import model.Owner;
import org.hibernate.SessionFactory;
import service.OwnerService;
import sessionUtil.HibernateSessionFactoryUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "owner", value = "/owners")
public class OwnersServlet extends HttpServlet {

    private final SessionFactory sessionFactory = HibernateSessionFactoryUtil.createSessionFactory();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Owner> owners = new OwnerService(new OwnerDAOImpl(sessionFactory)).getAllOwners();
        req.setAttribute("owners", owners);
        req.getRequestDispatcher("pages/owners.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            OwnerService ownerService = new OwnerService(new OwnerDAOImpl(sessionFactory));
            if (req.getParameter("removeId") != null) {
                ownerService.deleteOwner(ownerService.findOwnerById(Long.parseLong(req.getParameter("removeId"))));
            } else {
                String id = req.getParameter("id");
                String fio = req.getParameter("fio");
                String phone = req.getParameter("phone");
                String passportNumber = req.getParameter("passportNumber");

                Owner owner;

                if ("".equals(id)) {
                    owner = new Owner(fio, phone, passportNumber);
                    ownerService.saveOwner(owner);
                } else {
                    owner = ownerService.findOwnerById(Long.parseLong(id));
                    owner.setFio(fio);
                    owner.setPhone(phone);
                    owner.setPassportNumber(passportNumber);
                    ownerService.updateOwner(owner);
                }
            }
            resp.sendRedirect("owners");
        } catch (Exception e) {
            req.setAttribute("err", e.getMessage());
            doGet(req, resp);
        }
    }
}
