package servlets;

import dao.PriceListDAOImpl;
import model.PriceList;
import org.hibernate.SessionFactory;
import service.PriceListService;
import sessionUtil.HibernateSessionFactoryUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "priceList", value = "/priceList")
public class PriceListServlet extends HttpServlet {

    private final SessionFactory sessionFactory = HibernateSessionFactoryUtil.createSessionFactory();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<PriceList> priceLists = new PriceListService(new PriceListDAOImpl(sessionFactory)).getAllPriceList();
        req.setAttribute("priceLists", priceLists);
        req.getRequestDispatcher("pages/priceList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            PriceListService priceListService = new PriceListService(new PriceListDAOImpl(sessionFactory));
            if (req.getParameter("removeId") != null) {
                priceListService.deletePriceList(priceListService.findPriceListById(Long.parseLong(req.getParameter("removeId"))));
            } else {
                String id = req.getParameter("id");
                String workName = req.getParameter("name");
                String price = req.getParameter("price");

                PriceList priceList;

                if ("".equals(id)) {
                    priceList = new PriceList(workName, price);
                    priceListService.savePriceList(priceList);
                } else {
                    priceList = priceListService.findPriceListById(Long.parseLong(id));
                    priceList.setWorkName(workName);
                    priceList.setPrice(price);
                    priceListService.updatePriceList(priceList);
                }
            }
            resp.sendRedirect("priceList");
        } catch (Exception e) {
            req.setAttribute("err", e.getMessage());
            doGet(req, resp);
        }
    }
}
