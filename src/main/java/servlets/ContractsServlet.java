package servlets;

import dao.CarDAOImpl;
import dao.ContractDAOImpl;
import dao.OwnerDAOImpl;
import dao.PriceListDAOImpl;
import model.Car;
import model.Contract;
import model.Owner;
import model.PriceList;
import org.hibernate.SessionFactory;
import service.CarService;
import service.ContractService;
import service.OwnerService;
import service.PriceListService;
import sessionUtil.HibernateSessionFactoryUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "contract", value = "/contracts")
public class ContractsServlet extends HttpServlet {

    private final SessionFactory sessionFactory = HibernateSessionFactoryUtil.createSessionFactory();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        super.service(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Contract> contracts = new ContractService(new ContractDAOImpl(sessionFactory)).getAllContracts();
        req.setAttribute("contracts", contracts);

        List<Car> cars = new CarService(new CarDAOImpl(sessionFactory)).getAllCars();
        req.setAttribute("cars", cars);

        List<Owner> owners = new OwnerService(new OwnerDAOImpl(sessionFactory)).getAllOwners();
        req.setAttribute("owners", owners);

        List<PriceList> priceLists = new PriceListService(new PriceListDAOImpl(sessionFactory)).getAllPriceList();
        req.setAttribute("priceLists", priceLists);

        req.getRequestDispatcher("pages/contracts.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            CarService carService = new CarService(new CarDAOImpl(sessionFactory));
            OwnerService ownerService = new OwnerService(new OwnerDAOImpl(sessionFactory));
            ContractService contractService = new ContractService(new ContractDAOImpl(sessionFactory));
            PriceListService priceListService = new PriceListService(new PriceListDAOImpl(sessionFactory));

            if (req.getParameter("removeId") != null) {
                contractService.deleteContract((contractService.findContractById(Long.parseLong(req.getParameter("removeId")))));
            } else {
                String id = req.getParameter("id");
                String ownerFIO = req.getParameter("ownerList");
                String ownerCar = req.getParameter("carList");
                String[] listWorks = req.getParameterValues("listWorks");
                List<PriceList> worksList = new ArrayList<>();
                for (String work : listWorks) {
                    worksList.add(priceListService.findPriceListById(Long.parseLong(work)));
                }
                String startDate = req.getParameter("startDate");
                String endDate = req.getParameter("endDate");

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
                LocalDate startLocalDate = LocalDate.parse(startDate, formatter);
                LocalDate endLocalDate = LocalDate.parse(endDate, formatter);

                Car car = carService.findCarById(Long.parseLong(ownerCar));
                Owner owner = ownerService.findOwnerById(Long.parseLong(ownerFIO));

                Contract contract;

                if ("".equals(id)) {
                    contract = new Contract(startLocalDate, endLocalDate, car, owner, worksList);
                    contractService.saveContract(contract);
                } else {
                    contract = contractService.findContractById(Long.parseLong(id));
                    contract.setOwner(owner);
                    contract.setCar(car);
                    contract.setPriceLists(worksList);
                    contract.setStartDate(startLocalDate);
                    contract.setEndDate(endLocalDate);
                    contractService.updateContract(contract);
                }
            }
            resp.sendRedirect("contracts");
        } catch (Exception e) {
            req.setAttribute("err", e.getMessage());
            doGet(req, resp);
        }
    }
}
